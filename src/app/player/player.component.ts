import {ApplicationRef, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {PlayerService} from './player.service';
import {PlayerModel} from './player-model';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {
  private SONG_URL = '/assets/song.flac';
  private loading = false;
  public volume = 1;
  public eqSliders: any[];
  private barVisContext: CanvasRenderingContext2D;
  private sinVisContext: CanvasRenderingContext2D;

  private CANVAS_WIDTH = 640;
  private CANVAS_HEIGHT = 100;

  @ViewChild('barVisCanvas') barVisCanvas;
  @ViewChild('sinVisCanvas') sinVisCanvas;

  constructor (
    private player: PlayerService,
    private appRef: ApplicationRef,
    @Inject('Window') private window: Window
  ) {}

  ngOnInit() {
    this.eqSliders = PlayerModel.DEFAULT_EQ_NODE_DEFINITIONS.map((def: any) => {
      return {frequency: def.frequency, gain: PlayerModel.DEFAULT_EQ_NODE_GAIN};
    });

    this.barVisContext = this.barVisCanvas.nativeElement.getContext('2d');
    this.sinVisContext = this.sinVisCanvas.nativeElement.getContext('2d');
  }

  play(): Promise<any> {
    let promise = null;

    if (this.player.isPaused()) {
      promise = new Promise((resolve => resolve()));
    } else {
      promise = this._setSong();
    }

    return promise.then(() => this.player.play())
      .then(() => this._drawVis())
      .finally(() => this.setViewDirty());
  }

  _setSong(): Promise<any> {
    this.loading = true;

    return this.player.setSong(this.SONG_URL)
      .finally(() => this.loading = false);
  }

  pause(): Promise<void> {
    return this.player.pause()
      .finally(() => this.setViewDirty());
  }

  isPlaying(): boolean {
    return this.player.isPlaying();
  }

  setViewDirty(): void {
    this.appRef.tick();
  }

  setVolume(volume: number): void {
    this.player.setVolume(volume);
  }

  setEqGain(eqSlider: any): void {
    this.player.setEqGain(eqSlider.frequency, eqSlider.gain);
  }

  setEqFlat(): void {
    this.eqSliders.forEach((eq) => {
      eq.gain = 0;
      this.setEqGain(eq);
    });
  }

  setEqRadioOnTv(): void {
    this.eqSliders.forEach((eq) => {
      eq.gain = eq.frequency <= 440 ? -40 : 0;
      this.setEqGain(eq);
    });
  }

  setEqNextRoomOver(): void {
    this.eqSliders.forEach((eq) => {
      eq.gain = eq.frequency >= 1200 ? -40 : 0;
      this.setEqGain(eq);
    });
  }

  setEqCymbalsAndHiHats(): void {
    this.eqSliders.forEach((eq) => {
      eq.gain = eq.frequency >= 7000 ? 10 : -40;
      this.setEqGain(eq);
    });
  }

  _drawVis(): void {
    this._drawBarVis();
    this._drawSinVis();
  }

  _drawBarVis(): void {
    const bufferLength = this.player.getFrequencyBinCount();
    const dataArray = new Uint8Array(bufferLength);

    this.barVisContext.clearRect(0, 0, this.CANVAS_WIDTH, this.CANVAS_HEIGHT);

    const draw = () => {
      requestAnimationFrame(draw);

      this.player.getByteFrequencyData(dataArray);
      this.barVisContext.fillStyle = 'rgb(0, 0, 0)';
      this.barVisContext.fillRect(0, 0, this.CANVAS_WIDTH, this.CANVAS_HEIGHT);

      const barWidth = (this.CANVAS_WIDTH / bufferLength) * 2.5;
      let barHeight;
      let x = 0;

      for (let i = 0; i < bufferLength; i++) {
        barHeight = dataArray[i];

        this.barVisContext.fillStyle = `rgb(${barHeight + 100},50,50)`;
        this.barVisContext.fillRect(x, this.CANVAS_HEIGHT - barHeight / 2, barWidth, barHeight / 2);

        x += barWidth + 1;
      }
    };

    draw();
  }

  _drawSinVis(): void {
    const bufferLength = this.player.getFrequencyBinCount();
    const dataArray = new Uint8Array(bufferLength);

    this.sinVisContext.clearRect(0, 0, this.CANVAS_WIDTH, this.CANVAS_HEIGHT);

    const draw = () => {
      requestAnimationFrame(draw);

      this.player.getByteTimeDomainData(dataArray);
      this.sinVisContext.fillStyle = 'rgb(200, 200, 200)';
      this.sinVisContext.fillRect(0, 0, this.CANVAS_WIDTH, this.CANVAS_HEIGHT);
      this.sinVisContext.lineWidth = 2;
      this.sinVisContext.strokeStyle = 'rgb(0, 0, 0)';
      this.sinVisContext.beginPath();

      const sliceWidth = this.CANVAS_WIDTH / bufferLength;
      let x = 0;

      for (let i = 0; i < bufferLength; i++) {
        const v = dataArray[i] / 128.0;
        const y = v * this.CANVAS_HEIGHT / 2;

        if (i === 0) {
          this.sinVisContext.moveTo(x, y);
        } else {
          this.sinVisContext.lineTo(x, y);
        }

        x += sliceWidth;
      }

      this.sinVisContext.lineTo(this.CANVAS_WIDTH, this.CANVAS_HEIGHT / 2);
      this.sinVisContext.stroke();
    };

    draw();
  }
}
