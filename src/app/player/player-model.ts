export class PlayerModel {
  static DEFAULT_EQ_NODE_GAIN = 0.0;
  static DEFAULT_EQ_NODE_DEFINITIONS = [
    {frequency: 55, q: 2.5},
    {frequency: 77, q: 2.5},
    {frequency: 110, q: 2.5},
    {frequency: 156, q: 7},
    {frequency: 311, q: 2.5},
    {frequency: 440, q: 2},
    {frequency: 622, q: 2},
    {frequency: 880, q: 2},
    {frequency: 1200, q: 2},
    {frequency: 1800, q: 1.5},
    {frequency: 3500, q: 2},
    {frequency: 5000, q: 3},
    {frequency: 7000, q: 3},
    {frequency: 10000, q: 1},
    {frequency: 14000, q: 0.7},
    {frequency: 20000, q: 2.5},
  ];

  context: AudioContext;
  source: AudioBufferSourceNode;
  gainNode: GainNode;
  eqNodes: BiquadFilterNode[];
  nodes: AudioNode[];
  analyserNode: AnalyserNode;

  constructor(window: any) {
    this.context = new window.AudioContext();
    this.gainNode = this.context.createGain();
    this.analyserNode = this._createAnalyserNode();

    this.eqNodes = PlayerModel.DEFAULT_EQ_NODE_DEFINITIONS.map((def) => this._createEqNode(def));
    this.nodes = [this.gainNode];
    this.eqNodes.forEach((eqNode) => this.nodes.push(eqNode));
    this.nodes.push(this.analyserNode);

    this._connect();
  }

  _createAnalyserNode(): AnalyserNode {
    const node = this.context.createAnalyser();

    node.fftSize = 256;
    node.minDecibels = -90;
    node.maxDecibels = 90;
    node.smoothingTimeConstant = 0.85;

    return node;
  }

  _createEqNode(definition): BiquadFilterNode {
    const node = this.context.createBiquadFilter();

    node.type = 'peaking';
    node.frequency.value = definition.frequency;
    node.Q.value = definition.q;
    node.gain.value = PlayerModel.DEFAULT_EQ_NODE_GAIN;

    return node;
  }

  _connect() {
    this.source = this.context.createBufferSource();
    this.source.connect(this.nodes[0]);

    for (let x = 0; x < this.nodes.length - 1; x++) {
      this.nodes[x].connect(this.nodes[x + 1]);
    }

    this.nodes[this.nodes.length - 1].connect(this.context.destination);
  }

  setBuffer(buffer: AudioBuffer): void {
    this.source.buffer = buffer;
  }
}
