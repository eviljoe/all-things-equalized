import {Inject, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {PlayerModel} from './player-model';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  TRANSITION_TIME = 0.1;
  private model: PlayerModel;

  constructor(
    private http: HttpClient,
    @Inject('Window') private window: any
  ) {}

  _error(message: string, error: object) {
    this.window.alert(`"${message}"\n${JSON.stringify(error, null, 2)}`);
  }

  setSong(songUrl: string): Promise<any> {
    let promise = null;

    if (this.isPlaying() || this.isPaused()) {
      promise = this.stop().then(() => this._loadAndConnect(songUrl));
    } else {
      promise = this._loadAndConnect(songUrl);
    }

    return promise;
  }

  play(): Promise<void> {
    let promise = null;

    if (this.isPaused()) {
      promise = this.model.context.resume();
    } else if (this.model) {
      promise = new Promise((resolve) => {
        this.model.source.start(0);
        resolve();
      });
    } else {
      promise = new Promise((_resolve, reject) => reject());
    }

    return promise;
  }

  isPlaying(): boolean {
    return this.model && this.model.context.state === 'running';
  }

  pause(): Promise<void> {
    return this.model && this.model.context.suspend();
  }

  isPaused(): boolean {
    return this.model && this.model.context.state === 'suspended';
  }

  stop(): Promise<void> {
    return this.model.context.close();
  }

  setVolume(volume: number): void {
    if (this.model) {
      this.model.gainNode.gain.setTargetAtTime(volume, this.getCurrentTime(), this.TRANSITION_TIME / 3);
    }
  }

  setEqGain(frequency: number, gain: number): void {
    const node = this._getEqNode(frequency);

    if (node) {
      node.gain.setTargetAtTime(gain, this.getCurrentTime(), this.TRANSITION_TIME / 3);
    }
  }

  getCurrentTime(): number {
    return this.model ? this.model.context.currentTime : -1;
  }

  _getEqNode(frequency: number): BiquadFilterNode | null {
    return this.model ? this.model.eqNodes.find((node) => node.frequency.value === frequency) : null;
  }

  _loadAndConnect(songUrl: string): Promise<any> {
    this.model = new PlayerModel(this.window);

    return new Promise<any>((resolve, reject) => {
      this.http.get(songUrl, {responseType: 'arraybuffer'})
        .subscribe(
          (data: ArrayBuffer) => this._decodeArrayBuffer(this.model, data).then(resolve).catch(reject),
          (error: any) => {
            this._error(`Error downloading from "${songUrl}"`, error);
            reject(error);
          }
        );
    });
  }

  _decodeArrayBuffer(model: PlayerModel, songArrayBuffer: ArrayBuffer): Promise<AudioBuffer> {
    return model.context.decodeAudioData(
      songArrayBuffer,
      (buffer: AudioBuffer) => this.model.setBuffer(buffer),
      (error: any) => this._error(`Error decoding audio buffer.`, error)
    );
  }

  getByteFrequencyData(dataArray: Uint8Array): void {
    if (this.model) {
      this.model.analyserNode.getByteFrequencyData(dataArray);
    }
  }

  getFrequencyBinCount(): number {
    return this.model ? this.model.analyserNode.frequencyBinCount : 0;
  }

  getByteTimeDomainData(dataArray: Uint8Array): void {
    if (this.model) {
      this.model.analyserNode.getByteTimeDomainData(dataArray);
    }
  }
}
